#!/bin/sh

basedir=$(readlink -f $(dirname $0))
cd $basedir

cat > config.yaml <<EOF
base_url: http://localhost:8888
projects:
  - name: foo
    repository: $(pwd)/tmp/foo.git
    build_command: 'ruby -e "ENV.each { |k,v| puts [k,v].inspect if k =~ /HEAD/ }; exit(rand(2))"'
    commit_url: https://gitlab.com/noosfero/noosfero/commit/%s
    compare_url: https://gitlab.com/noosfero/noosfero/compare/%s...%s
EOF

# create/augment fake repo
mkdir -p tmp/foo.git
cd tmp
if [ ! -f foo.git/config ]; then
  git init --bare foo.git
fi
if [ ! -d foo ]; then
  git clone foo.git foo
fi
cd foo
date > README
git add README
git commit -m 'one commit more'
git push

cd $basedir
./noosfero-ci-wrapper
